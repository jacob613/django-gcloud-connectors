# Django GCloud Connectors (gcloudc)

Django Google Cloud Connectors provides database backends for two non-relational database services that run on Google Cloud:

 - Google Cloud Datastore
 - Google Cloud Firestore

This django-gcloud-connectors project allows you to seamlessly use the Django ORM with these databases; allowing you to leverage
the full power of Django on a massively scalable No-SQL database.

For more information on the project, please see the [documentation](https://potato-oss.gitlab.io/google-cloud/django-gcloud-connectors/)

If you are interested in submitting a patch, please refer to the [Contributing](https://potato-oss.gitlab.io/google-cloud/django-gcloud-connectors/contributing) page.

---

## Looking for Commercial Support?

Potato offers Commercial Support for all its Open Source projects and we can tailor a support package to your needs.

If you're interested in commercial support, training, or consultancy then go ahead and contact us at [opensource@potatolondon.com](mailto:opensource@potatolondon.com)
